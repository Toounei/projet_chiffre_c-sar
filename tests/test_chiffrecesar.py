import unittest
from chiffrecesar import uncircular_chiffrer_caractere, uncircular_dechiffrer_chaine, uncircular_chiffrer_chaine, uncircular_dechiffrer_caractere   # Cette ligne permet d'importer la fonction chiffrer_caractere au sein de notre fichier de test
from chiffrecesar import circular_chiffrer_caractere, circular_dechiffrer_caractere, circular_chiffrer_chaine, circular_dechiffrer_chaine
class TestChiffreCesar(unittest.TestCase):

    def test_uncircular_chiffrer_caractere(self) :
        self.assertEqual(uncircular_chiffrer_caractere(ord("a"), 8), "i")
        self.assertEqual(uncircular_chiffrer_caractere(ord("["), 9), "d")
        self.assertEqual(uncircular_chiffrer_caractere(ord("_"), 10), "i")
        self.assertEqual(uncircular_chiffrer_caractere(ord("A"), 11), "L")
    
    def test_uncircular_chiffrer_chaine(self) :
        self.assertEqual(uncircular_chiffrer_chaine("aa", 8), "ii")
        self.assertEqual(uncircular_chiffrer_chaine("la", 9), "uj")
        self.assertEqual(uncircular_chiffrer_chaine("toto", 10), "~y~y")
        self.assertEqual(uncircular_chiffrer_chaine("/_(", 11), ":j3")

    def test_uncircular_dechiffrer_caractere(self) :
        self.assertEqual(uncircular_dechiffrer_caractere(ord("$"), 9), chr(36-9)) ### 36 = $
        self.assertEqual(uncircular_dechiffrer_caractere(ord(chr(128)), 10), "v") ### 128 = signe euro
        self.assertEqual(uncircular_dechiffrer_caractere(ord("7"), 11), ",")

    def test_uncircular_dechiffrer_chaine(self) :
        self.assertEqual(uncircular_dechiffrer_chaine("opq", 9), "fgh")
        self.assertEqual(uncircular_dechiffrer_chaine("[\]", 10), "QRS")
        self.assertEqual(uncircular_dechiffrer_chaine("ÀÁÂ", 8), "¸¹º")

    def test_circular_chiffrer_caractere(self) :
        self.assertEqual(circular_chiffrer_caractere(ord("a"), 13), "n")
        self.assertEqual(circular_chiffrer_caractere(ord("A"), 12), "M")
        self.assertEqual(circular_chiffrer_caractere(ord("r"), 1), "s")
        self.assertEqual(circular_chiffrer_caractere(ord("z"), 1), "a")

    def test_circular_chiffrer_chaine(self) :
        self.assertEqual(circular_chiffrer_chaine("oklm", 13), "bxyz")
        self.assertEqual(circular_chiffrer_chaine("BENJI", 12), "NQZVU")
        self.assertEqual(circular_chiffrer_chaine("menace)", 10), "woxkmo)")

    def test_circular_dechiffrer_caractere(self):
        self.assertEqual(circular_dechiffrer_caractere(ord("a"), 13), "n")
        self.assertEqual(circular_dechiffrer_caractere(ord("A"), 12), "O")
        self.assertEqual(circular_dechiffrer_caractere(ord("r"), 1), "q")
        self.assertEqual(circular_dechiffrer_caractere(ord("a"), 1), "z")        

    def test_circular_dechiffrer_chaine(self):
        self.assertEqual(circular_dechiffrer_chaine("oklm", 13), "bxyz")
        self.assertEqual(circular_dechiffrer_chaine("BENJI", 12), "PSBXW")
        self.assertEqual(circular_dechiffrer_chaine("menace)", 10), "cudqsu)")
