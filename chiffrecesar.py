from tkinter import Radiobutton, Button, Text, Label, Tk, END, Menu, PhotoImage, Canvas
from tkinter.messagebox import showinfo, showwarning

color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"


def uncircular_chiffrer_caractere(caractere, cle) :
    """
    Retourne un caractere chiffrer avec l'entier cle
    """
    return chr(caractere + cle)

def uncircular_dechiffrer_caractere(caractere, cle) :
    """
    Retourne un caractere déchiffrer avec l'entier cle
    """
    return chr(caractere - cle)

def uncircular_chiffrer_chaine(chaine_caractere, cle) :
    """
    Retourne une chaine de caractere chiuffrer
    """ 
    result = ""
    for caract in chaine_caractere :
        new_caract = uncircular_chiffrer_caractere(ord(caract), cle) #Utilisation de la fonction 'chiffrer_caractere' pour chaque caractere
        result = result + new_caract
    
    return result

def uncircular_dechiffrer_chaine(chaine_caractere, cle) :
    """
    Retourne une chaine de caractere déchiffrer
    """
    result = ""
    for caract in chaine_caractere :
        new_caract = uncircular_dechiffrer_caractere(ord(caract), cle) #Utilisation de la fonction 'dechiffrer_caractere' pour chaque caractere
        result = result + new_caract
    
    return result

def circular_chiffrer_caractere(caractere, cle):
    """
    Retourne un caractere chiffrer avec la méthode circulaire grâce à la cle
    """
    min_max = 0
    if caractere >= 97 :
        min_max = 97
        caractere = caractere - 97
    
    if caractere >= 65 :
        min_max = 65
        caractere = caractere - 65
    
    caractere = (caractere + cle)%26
    caractere = caractere + min_max
    return chr(caractere)

def circular_dechiffrer_caractere(caractere, cle):
    """
    Retourne un caractere dechiffrer avec la methode circulaire grâce à la cle
    """
    min_max = 0
    if caractere >= 97 :
        min_max = 97
        caractere = caractere - 97
    
    if caractere >= 65 :
        min_max = 65
        caractere = caractere - 65
    
    caractere = (caractere - cle)%26
    caractere = caractere + min_max
    return chr(caractere)

def circular_chiffrer_chaine(chaine_caractere, cle):
    """
    Retourne une chaine de caractere chiffrer de façon circulaure
    """
    result = ""
    for caract in chaine_caractere:
        if caract.islower():
            result = result + circular_chiffrer_caractere(ord(caract),cle)
        elif caract.isupper():
            result = result + circular_chiffrer_caractere(ord(caract),cle)
        else: 
            result = result + caract
    return result

def circular_dechiffrer_chaine(chaine_caractere, cle):
    """
    Retourne une chaine de caractere chiffrer de façon circulaure
    """
    result = ""
    for caract in chaine_caractere:
        if caract.islower():
            result = result + circular_dechiffrer_caractere(ord(caract),cle)
        elif caract.isupper():
            result = result + circular_dechiffrer_caractere(ord(caract),cle)
        else:
            result = result + caract
    return result


def check_textarea_content():
    """
    Check textarea_content and raise a warning if needed
    :return: if texterea contains a valid string
    :rtype: bool 
    """
    string = get_textarea_content()
    if len(string) == 0:
        showwarning(title="Ooopsii", message="Please provide a text !")
        return False
    return True

def set_textarea_content(string):
    """
    Change le textarea
    """
    textarea.delete('0.0', END)
    textarea.insert(END, string)

def get_textarea_content():
    """
    Return text from textarea component striped
    """
    return textarea.get("0.0", END).strip()

def callback_chiffrer():
    """
    Callback pour chiffrer la chaine
    """
    if check_textarea_content():
        string = get_textarea_content()
        set_textarea_content(chiffrer_chaine(string))

def callback_dechiffrer():
    """
    Callback pour dechiffrer la chaine
    """
    if check_textarea_content():
        string = get_textarea_content()
        set_textarea_content(dechiffrer_chaine(string))

if __name__ == '__main__':
    """
    World famous words counter

    1. Creation du tkinter
    2. Ajout des fonctionnalité
    """
    frame = Tk()
    frame.title("Chiffre de Cesar")
    frame.minsize(500, 450)

    frame.rowconfigure(1, weight=1)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)
    frame.columnconfigure(2, weight=1)

    menu = Menu(frame)
    file_menu = Menu(menu, tearoff=0)
    file_menu.add_command(label='Chiffrer', command=chiffrer_chaine)
    file_menu.add_command(label='Déchiffrer', command=dechiffrer_chaine)
    file_menu.add_separator()
    file_menu.add_command(label='Quit', command=frame.quit)
    menu.add_cascade(label='File', menu=file_menu)
    frame.config(menu=menu)

    textarea = Text(frame,
                    wrap='word',
                    bg=color_white,
                    fg=color_black)
    textarea.grid(row=1, columnspan=3, padx=10, pady=10, sticky='nsew')

    btn_chiffrer_chaine = Button(frame,
                             compound="left",
                             text='Chiffrer la chaine',
                             command=callback_chiffrer,
                             bg=color_blue,
                             activebackground=color_lightblue)
    btn_chiffrer_chaine.grid(row=2, column=0)

    btn_dechiffrer_chaine = Button(frame,
                             compound="left",
                             text='Dechiffrer la chaine',
                             command=callback_dechiffrer,
                             bg=color_grey,
                             activebackground=color_lightgrey,
                             fg=color_black,
                             activeforeground=color_black)
    btn_dechiffrer_chaine.grid(row=2, column=1)

    btn_quit = Button(frame,
                      text="Exit",
                      command=frame.quit,
                      bg=color_red,
                      activebackground=color_lightred,
                      fg=color_black,
                      activeforeground=color_black)
    btn_quit.grid(row=2, column=2)

    frame.mainloop()
